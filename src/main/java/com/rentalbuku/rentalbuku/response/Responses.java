package com.rentalbuku.rentalbuku.response;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class Responses {

    /**
     * A static factory to create error response to the client
     * @param httpStatus http status code of error
     * @param error error message area
     * @param message description of the error if exists
     * @return ResponseEntity<Response> with status code given in httpStatus param
     */
    public static ResponseEntity<Response> ofError (HttpStatus httpStatus, String error, String message) {
        return ResponseEntity.status(httpStatus).body(new ErrorResponse(httpStatus, message, error));
    }

    /**
     * Accept string message and object as data and return as ResponseEntity<Response> and status code 200
     * @param data data will be send to client
     * @param message message attach to response
     * @return ResponseEntity<Response> with status code 200
     */
    public static ResponseEntity<Response> ofData (Object data, String message) {
        return ResponseEntity.ok(new DataResponse(HttpStatus.OK, message, data));
    }

    /**
     * Accept object as data and return as ResponseEntity<Response> and status code 200
     * @param data data will be send to client
     * @return ResponseEntity<Response> with status code 200
     */
    public static ResponseEntity<Response> ofData (Object data) {
        return ResponseEntity.ok(new DataResponse(HttpStatus.OK, "-", data));
    }

    public static ResponseEntity<Response> ofBadRequest (Object error, String message) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorResponse(HttpStatus.BAD_REQUEST, message, error));
    }

    public static ResponseEntity<Response> ofBadRequest (Object error) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorResponse(HttpStatus.BAD_REQUEST, "-", error));
    }
}
