package com.rentalbuku.rentalbuku.service;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.rentalbuku.rentalbuku.entity.Transaction;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {

	@Query(value = "INSERT INTO transaction (created_at, fee_total, return_status, user_id) \r\n"
			+ "VALUES (NOW(), ?1, 0, ?2)", nativeQuery = true)
	int NewTransaction(Integer price, Long user_id);
	
	@Query(value = "SELECT SUM(trd.qty)\r\n"
			+ "FROM transaction tr\r\n"
			+ "JOIN transaction_detail trd ON tr.id = trd.transaction_id\r\n"
			+ "WHERE tr.return_status = 0", nativeQuery = true)
	int GetUnreturnedBooksTotal();
	
	@Query(value = "SELECT SUM(trd.qty)\r\n"
			+ "FROM transaction tr\r\n"
			+ "JOIN transaction_detail trd ON tr.id = trd.transaction_id\r\n"
			+ "WHERE tr.return_status = 1", nativeQuery = true)
	int GetReturnedBooksTotal();
	
	@Query(value = "SELECT SUM(DISTINCT tr.fee_total) AS totalIncome\r\n"
			+ "FROM transaction tr\r\n"
			+ "JOIN transaction_detail trd ON tr.id = trd.transaction_id", nativeQuery = true)
	int GetTotalIncome();
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE transaction SET return_status = 1 WHERE id = ?1", nativeQuery = true)
	int UpdateBookReturnedStatus(Long id);
	
	@Query(value = "SELECT SUM(td.qty)\r\n"
			+ "FROM transaction tr\r\n"
			+ "JOIN transaction_detail td ON td.transaction_id = tr.id\r\n"
			+ "JOIN user u ON tr.user_id = u.id\r\n"
			+ "WHERE u.id = ?1", nativeQuery = true)
	int GetTotalBookMember(Long user_id);
	
	@Query(value = "SELECT SUM(DISTINCT tr.fee_total)\r\n"
			+ "FROM transaction tr\r\n"
			+ "JOIN transaction_detail td ON td.transaction_id = tr.id\r\n"
			+ "JOIN user u ON tr.user_id = u.id\r\n"
			+ "WHERE u.id = ?1 AND tr.return_status = 1", nativeQuery = true)
	int GetTotalSpendMember(Long user_id);
	
	@Query(value = "SELECT SUM(td.qty)\r\n"
			+ "FROM transaction tr\r\n"
			+ "JOIN transaction_detail td ON td.transaction_id = tr.id\r\n"
			+ "JOIN user u ON tr.user_id = u.id\r\n"
			+ "WHERE u.id = ?1 AND tr.return_status = 0 AND tr.created_at BETWEEN NOW() - INTERVAL 30 DAY AND NOW()", nativeQuery = true)
	int GetUnreturnedBookMemberOneMonth();
}
