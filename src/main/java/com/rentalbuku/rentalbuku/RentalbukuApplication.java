package com.rentalbuku.rentalbuku;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RentalbukuApplication {

	public static void main(String[] args) {
		SpringApplication.run(RentalbukuApplication.class, args);
	}

}
