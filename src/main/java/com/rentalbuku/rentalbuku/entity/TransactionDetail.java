package com.rentalbuku.rentalbuku.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;

@Entity
@Table(name = "transaction_detail")
public class TransactionDetail {
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private Integer price;
	private Integer qty;

	@Column(name = "CREATED_AT", nullable = false)
	@CreationTimestamp
	private Timestamp created_at;

	@Column(name = "UPDATED_AT")
	private Timestamp updated_at;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "transaction_id", referencedColumnName = "id", nullable = false, 
				foreignKey = @ForeignKey(name = "FK_transaction_transDetail"))
	private Transaction transaction;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "book_id", referencedColumnName = "id", nullable = false, foreignKey = @ForeignKey(name = "FK_book_id"))
	private Book book;

	public TransactionDetail() {
	}

	public TransactionDetail(Integer price, Integer qty, Transaction transaction, Book book) {
		super();
		this.price = price;
		this.qty = qty;
		this.transaction = transaction;
		this.book = book;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public Integer getQty() {
		return qty;
	}

	public void setQty(Integer qty) {
		this.qty = qty;
	}

	public Timestamp getCreated_at() {
		return created_at;
	}

	public void setCreated_at(Timestamp created_at) {
		this.created_at = created_at;
	}

	public Timestamp getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(Timestamp updated_at) {
		this.updated_at = updated_at;
	}

	public Transaction getTransaction() {
		return transaction;
	}

	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	@Override
	public String toString() {
		return "TransactionDetail [id=" + id + ", price=" + price + ", qty=" + qty + ", created_at=" + created_at
				+ ", updated_at=" + updated_at + ", transaction=" + transaction + ", book=" + book + "]";
	}

}
