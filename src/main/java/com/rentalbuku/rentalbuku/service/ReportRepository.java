package com.rentalbuku.rentalbuku.service;

import java.sql.Timestamp;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.rentalbuku.rentalbuku.entity.Report;

public interface ReportRepository extends JpaRepository<Report, Long>{
	
	@Query(value = "INSERT INTO report (user_name, book_qty, rent_fee, rent_date, rent_total, transaction, created_at) \r\n"
			+ "VALUES (?1, ?2, ?3, ?4, ?5, ?6, NOW());", nativeQuery = true)
	int NewReport(String userName, Integer bookQty, Integer rentFee, Timestamp rentDate, Integer rentTotal, Long transactionId);
}
