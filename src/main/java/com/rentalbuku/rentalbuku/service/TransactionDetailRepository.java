package com.rentalbuku.rentalbuku.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.rentalbuku.rentalbuku.entity.TransactionDetail;

public interface TransactionDetailRepository extends JpaRepository<TransactionDetail, Long>{
	@Transactional
	@Modifying
	@Query(value = "INSERT INTO transaction_detail (book_id, transaction_id, price, qty, created_at) \r\n"
			+ "VALUES (?1, ?2, ?3, ?4, NOW());", nativeQuery = true)
	int NewTransactionDetail(Long book_id, Long transaction_id, Integer price, Integer qty);
	
	@Query(value = "SELECT id, book_id, transaction_id, price, qty, created_at, updated_at\r\n"
			+ "FROM transaction_detail", nativeQuery = true)
	List<TransactionDetail> GetAllTransactionDetail();
	
	@Query(value = "SELECT id, book_id, transaction_id, price, qty, created_at, updated_at\r\n"
			+ "FROM transaction_detail\r\n"
			+ "WHERE transaction_id = ?1", nativeQuery = true)
	List<TransactionDetail> GetTransactionDetailById(Long id);
}
