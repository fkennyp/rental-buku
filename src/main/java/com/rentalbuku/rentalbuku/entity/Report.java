package com.rentalbuku.rentalbuku.entity;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.CreationTimestamp;

@Entity
public class Report {
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "user_name")
	private String userName;
	
	@Column(name = "book_qty")
	private Integer bookQty;
	
	@Column(name = "rent_date")
	private LocalDateTime rentDate;
	
	@Column(name = "return_date")
	private LocalDateTime returnDate;
	
	@Column(name = "rent_fee")
	private Integer rentFee;

	@Column(name = "rent_fine")
	private Integer rentFine;
	
	@Column(name = "rent_total")
	private Integer rentTotal;

	@Column(name = "CREATED_AT", nullable = false)
	@CreationTimestamp
	private Timestamp created_at;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "transaction", referencedColumnName = "id", 
				nullable = false, foreignKey = @ForeignKey(name = "FK_transaction_report"))
	private Transaction transaction;
	
	public Report () {}

	public Report(String userName, Integer bookQty, LocalDateTime rentDate, LocalDateTime returnDate, Integer rentFee,
			Integer rentFine, Integer rentTotal, Transaction transaction) {
		super();
		this.userName = userName;
		this.bookQty = bookQty;
		this.rentDate = rentDate;
		this.returnDate = returnDate;
		this.rentFee = rentFee;
		this.rentFine = rentFine;
		this.rentTotal = rentTotal;
		this.transaction = transaction;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Integer getBookQty() {
		return bookQty;
	}

	public void setBookQty(Integer bookQty) {
		this.bookQty = bookQty;
	}

	public LocalDateTime getRentDate() {
		return rentDate;
	}

	public void setRentDate(LocalDateTime rentDate) {
		this.rentDate = rentDate;
	}

	public LocalDateTime getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(LocalDateTime returnDate) {
		this.returnDate = returnDate;
	}

	public Integer getRentFee() {
		return rentFee;
	}

	public void setRentFee(Integer rentFee) {
		this.rentFee = rentFee;
	}

	public Integer getRentFine() {
		return rentFine;
	}

	public void setRentFine(Integer rentFine) {
		this.rentFine = rentFine;
	}

	public Integer getRentTotal() {
		return rentTotal;
	}

	public void setRentTotal(Integer rentTotal) {
		this.rentTotal = rentTotal;
	}

	public Timestamp getCreated_at() {
		return created_at;
	}

	public void setCreated_at(Timestamp created_at) {
		this.created_at = created_at;
	}

	public Transaction getTransaction() {
		return transaction;
	}

	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	@Override
	public String toString() {
		return "Report [id=" + id + ", userName=" + userName + ", bookQty=" + bookQty + ", rentDate=" + rentDate
				+ ", returnDate=" + returnDate + ", rentFee=" + rentFee + ", rentFine=" + rentFine + ", rentTotal="
				+ rentTotal + ", created_at=" + created_at + ", transaction=" + transaction + "]";
	}
	
}
