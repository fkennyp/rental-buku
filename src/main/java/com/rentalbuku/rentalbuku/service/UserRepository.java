package com.rentalbuku.rentalbuku.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.rentalbuku.rentalbuku.dto.CustomUser;
import com.rentalbuku.rentalbuku.entity.User;

public interface UserRepository extends JpaRepository<User, Long>{
	@Query(value = "SELECT u.id, u.name, u.email, u.phone, u.address, s.name AS statusName \r\n"
			+ "FROM user u\r\n"
			+ "JOIN status s ON u.status_id = s.id\r\n", nativeQuery = true)
	List<CustomUser.GetUser> GetUsers();
	
	@Transactional
	@Modifying
	@Query(value = "INSERT INTO user (username, name, password, address, phone, email, status_id, register_date) "
			+ "VALUES (?1, ?2, \"adminBookRental\", ?3, ?4, ?5, 1, NOW())", nativeQuery = true)
	int NewAdmin(String username, String name, String address, String phone, String email);
	
	@Query(value = "SELECT u.id, u.name, u.email, u.phone, u.address, s.name AS statusName\r\n"
			+ "FROM user u\r\n"
			+ "JOIN status s ON u.status_id = s.id\r\n"
			+ "WHERE u.phone = ?1", nativeQuery = true)
	List<CustomUser.GetUser> findUserByPhone(String phone);
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE user SET status_id = 2 WHERE id = ?1", nativeQuery = true)
	int StatusPublicToMember(Long user_id);
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE user SET password = 'customerBookRental' WHERE id = ?1", nativeQuery = true)
	int ResetPasswordCustomer(Long user_id);
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE user SET password = 'adminBookRental' WHERE id = ?1", nativeQuery = true)
	int ResetPasswordAdmin(Long user_id);
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE user SET name = ?1, address = ?2, phone = ?3, status_id = 2 WHERE id = ?4", nativeQuery = true)
	int UpdateUser(String name, String address, String phone, Long id);
}
