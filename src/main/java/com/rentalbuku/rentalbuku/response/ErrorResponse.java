package com.rentalbuku.rentalbuku.response;

import org.springframework.http.HttpStatus;

public class ErrorResponse implements Response {
	private int status;
	private String message;
	private Object error;

	public ErrorResponse(HttpStatus status, String message, String error) {
		this.status = status.value();
		this.message = message;
		this.error = error;
	}

	public ErrorResponse(HttpStatus status, String message, Object error) {
		this.status = status.value();
		this.message = message;
		this.error = error;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(HttpStatus status) {
		this.status = status.value();
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getError() {
		return error;
	}

	public void setError(Object error) {
		this.error = error;
	}

}
