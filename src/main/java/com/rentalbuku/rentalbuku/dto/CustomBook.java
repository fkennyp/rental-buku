package com.rentalbuku.rentalbuku.dto;

public interface CustomBook {
	public interface GetBooksDetails{
		public String getTitle();
		public String getAuthor();
		public String getDescriptions();
		public Integer getQty();
		public Integer getRentPrice();
		public String getBookType();
	}
}
