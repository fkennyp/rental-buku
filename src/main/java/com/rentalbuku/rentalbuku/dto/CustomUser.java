package com.rentalbuku.rentalbuku.dto;

public interface CustomUser {
	public interface GetUser {
		public Long getId();
		public String getName();
		public String getEmail();
		public String getPhone();
		public String getAddress();
		public String getStatusName();
	}
}
