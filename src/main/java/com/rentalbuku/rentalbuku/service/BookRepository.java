package com.rentalbuku.rentalbuku.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.rentalbuku.rentalbuku.dto.CustomBook;
import com.rentalbuku.rentalbuku.entity.Book;

public interface BookRepository extends JpaRepository<Book, Long> {
	@Query(value = "SELECT b.id, b.title, b.author, b.description, b.rent_price, b.qty, b.book_type_id, \r\n"
			+ "		b.created_at, b.updated_at, b.deleted_at\r\n" + "FROM book b\r\n"
			+ "JOIN book_type bt ON b.book_type_id = bt.id", nativeQuery = true)
	List<Book> GetBooks();
	
	@Query(value = "SELECT b.id, b.title, b.author, b.descriptions, b.qty, b.rent_price AS rentPrice, bt.type_name AS bookType\r\n"
			+ "FROM book b\r\n"
			+ "JOIN book_type bt ON bt.id = b.book_type_id", nativeQuery = true)
	List<CustomBook.GetBooksDetails> GetBooksDetails();
	
	@Query(value = "SELECT b.id, b.title, b.author, b.descriptions, b.qty, b.rent_price AS rentPrice, bt.type_name AS bookType\r\n"
			+ "FROM book b\r\n"
			+ "JOIN book_type bt ON bt.id = b.book_type_id "
			+ "WHERE b.id = ?1", nativeQuery = true)
	List<CustomBook.GetBooksDetails> GetBooksDetailsById(Long book_id);

	@Query(value = "SELECT b.id, b.title, b.author, b.description, b.rent_price, b.qty, b.book_type_id, \r\n"
			+ "		b.created_at, b.updated_at, b.deleted_at, bt.type_name\r\n" + "FROM book b\r\n"
			+ "JOIN book_type bt ON b.book_type_id = bt.id\r\n" + "WHERE b.id = ?1", nativeQuery = true)
	Book GetBookById(Long id);

	@Transactional
	@Modifying
	@Query(value = "INSERT INTO book (title, author, descriptions, rent_price, qty, book_type_id, created_at) "
			+ "VALUES (?1, ?2, ?3, ?4, ?5, ?6, NOW())", nativeQuery = true)
	int AddBook(String title, String author, String description, int rent_price, int qty, Long book_type_id);
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE book SET qty = qty + ?2 WHERE id = ?1", nativeQuery = true)
	int UpdateBookQty(Long id, Integer qty);
}
