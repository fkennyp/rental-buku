package com.rentalbuku.rentalbuku.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.rentalbuku.rentalbuku.dto.CustomUser;
import com.rentalbuku.rentalbuku.dto.NewAdminDto;
import com.rentalbuku.rentalbuku.entity.User;
import com.rentalbuku.rentalbuku.response.Response;
import com.rentalbuku.rentalbuku.response.Responses;
import com.rentalbuku.rentalbuku.service.UserRepository;

@RestController
public class AdminController {
	@Autowired
	UserRepository userRepository;
	
	@GetMapping(value = "/admin/users")
	public ResponseEntity<Response> GetUsers(){
		List<CustomUser.GetUser> users = userRepository.GetUsers();
		return Responses.ofData(users);
	}
	
	@PostMapping(value = "/admin/users/add")
	public ResponseEntity<Response> NewAdmin(@RequestBody NewAdminDto payload ){
		String username = payload.getUsername();
		String name = payload.getName();
		String address = payload.getAddress();
		String phone = payload.getPhone();
		String email = payload.getEmail();
		
		int result = userRepository.NewAdmin(username, name, address, phone, email);
		List<CustomUser.GetUser> newAdmin = userRepository.findUserByPhone(phone);
		return Responses.ofData(newAdmin);
	}
}
