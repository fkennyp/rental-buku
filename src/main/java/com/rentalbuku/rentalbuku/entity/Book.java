package com.rentalbuku.rentalbuku.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;

@Entity
@Table(name = "book")
public class Book {
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "TITLE")
	private String title;

	@Column(name = "AUTHOR")
	private String author;

	@Column(name = "QTY")
	private Integer qty;

	@Column(name = "RENT_PRICE")
	private Integer rentPrice;

	@Column(name = "DESCRIPTIONS")
	private String descriptions;

	@Column(name = "CREATED_AT", nullable = false)
	@CreationTimestamp
	private Timestamp created_at;

	@Column(name = "UPDATED_AT")
	private Timestamp updated_at;

	@Column(name = "DELETED_AT")
	private Timestamp deleted_at;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "book_type_id", referencedColumnName = "id", 
			nullable = false, foreignKey = @ForeignKey(name = "FK_book_type"))
	private BookType bookType;

	public Book() {}
	
	public Book(String title, String author, Integer qty, Integer rentPrice, String description, BookType bookType) {
		super();
		this.title = title;
		this.author = author;
		this.qty = qty;
		this.rentPrice = rentPrice;
		this.descriptions = descriptions;
		this.bookType = bookType;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public Integer getQty() {
		return qty;
	}

	public void setQty(Integer qty) {
		this.qty = qty;
	}

	public Integer getRentPrice() {
		return rentPrice;
	}

	public void setRentPrice(Integer rentPrice) {
		this.rentPrice = rentPrice;
	}

	public String getDescriptions() {
		return descriptions;
	}

	public void setDescription(String description) {
		this.descriptions = description;
	}

	public Timestamp getCreated_at() {
		return created_at;
	}

	public void setCreated_at(Timestamp created_at) {
		this.created_at = created_at;
	}

	public Timestamp getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(Timestamp updated_at) {
		this.updated_at = updated_at;
	}

	public Timestamp getDeleted_at() {
		return deleted_at;
	}

	public void setDeleted_at(Timestamp deleted_at) {
		this.deleted_at = deleted_at;
	}

	public BookType getBookType() {
		return bookType;
	}

	public void setBookType(BookType bookType) {
		this.bookType = bookType;
	}

	@Override
	public String toString() {
		return "Book [id=" + id + ", title=" + title + ", author=" + author + ", qty=" + qty + ", rentPrice="
				+ rentPrice + ", descriptions=" + descriptions + ", created_at=" + created_at + ", updated_at="
				+ updated_at + ", deleted_at=" + deleted_at + ", bookType=" + bookType + "]";
	}
	
	
}
