package com.rentalbuku.rentalbuku.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.rentalbuku.rentalbuku.dto.CustomBook;
import com.rentalbuku.rentalbuku.entity.Book;
import com.rentalbuku.rentalbuku.response.Response;
import com.rentalbuku.rentalbuku.response.Responses;
import com.rentalbuku.rentalbuku.service.BookRepository;

@RestController
public class BookController {
	@Autowired
	BookRepository bookRepository;

	@GetMapping(value = "/admin/books", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response> GetBooks() {
		List<Book> books = bookRepository.GetBooks();

		return Responses.ofData(books);
	}

	@PostMapping(value = "/admin/books/add", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response> AddBook(@RequestBody Book payload) {
		int result = bookRepository.AddBook(payload.getTitle(), payload.getAuthor(), payload.getDescriptions(), payload.getRentPrice(), payload.getQty(), payload.getBookType().getId());
		System.out.println(result);
		return Responses.ofData(payload);
		
		
	}
	
//	@GetMapping(value = "/hehe", produces = MediaType.APPLICATION_JSON_VALUE)
//	public ResponseEntity<Response> GetBooksDetails(){
//		List<CustomBook.GetBooksDetails> booksDetails = bookRepository.GetBooksDetailsById(6L); 
//		return Responses.ofData(booksDetails);
//	}
}
