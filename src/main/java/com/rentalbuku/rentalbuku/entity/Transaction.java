package com.rentalbuku.rentalbuku.entity;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;

@Entity
@Table(name = "transaction")
public class Transaction {
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "return_date")
	private LocalDateTime returnDate;

	@Column(name = "return_status")
	private Boolean returnStatus;

	@Column(name = "fee_total")
	private Integer feeTotal;

	@Column(name = "CREATED_AT", nullable = false)
	@CreationTimestamp
	private Timestamp created_at;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user_id", referencedColumnName = "id", 
				nullable = false, foreignKey = @ForeignKey(name = "FK_user_id"))
	private User user;

	public Transaction() {
	}

	public Transaction(LocalDateTime returnDate, Boolean returnStatus, Integer feeTotal, User user) {
		super();
		this.returnDate = returnDate;
		this.returnStatus = returnStatus;
		this.feeTotal = feeTotal;
		this.user = user;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDateTime getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(LocalDateTime returnDate) {
		this.returnDate = returnDate;
	}

	public Boolean getReturnStatus() {
		return returnStatus;
	}

	public void setReturnStatus(Boolean returnStatus) {
		this.returnStatus = returnStatus;
	}

	public Integer getFeeTotal() {
		return feeTotal;
	}

	public void setFeeTotal(Integer feeTotal) {
		this.feeTotal = feeTotal;
	}

	public Timestamp getCreated_at() {
		return created_at;
	}

	public void setCreated_at(Timestamp created_at) {
		this.created_at = created_at;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "Transaction [id=" + id + ", returnDate=" + returnDate + ", returnStatus=" + returnStatus + ", feeTotal="
				+ feeTotal + ", created_at=" + created_at + ", user=" + user + "]";
	}

}
